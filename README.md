# CatCat聊天服务端
ChatCat聊天(服务端)，Swing+Socket(TCP)，JavaSE课程项目。
## 主要界面展示
![](https://i.imgur.com/3MIkpZY.jpg)
## 开发与备注日志
### 2018年8月12日
【1】今日添加README文件。
<br>
【2】ChatCat的客户端、数据库、服务端分离在不同的主机上，需要一台路由器建立局域网，在局域网内实现多线程聊天。
<br>
【3】客户端与数据库见[ChatCat聊天客户端](https://github.com/LauZyHou/ChatCat_Server)。
